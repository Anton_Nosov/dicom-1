﻿namespace Dicom_1
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.trackBarWidth = new System.Windows.Forms.TrackBar();
            this.trackBarCenter = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelWidthTitle = new System.Windows.Forms.Label();
            this.labelCenterTitle = new System.Windows.Forms.Label();
            this.labelWidthValue = new System.Windows.Forms.Label();
            this.labelCenterValue = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCenter)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Filter = "DICOM Files(*.dcm)|*.dcm";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(950, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьФайлToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьФайлToolStripMenuItem
            // 
            this.открытьФайлToolStripMenuItem.Name = "открытьФайлToolStripMenuItem";
            this.открытьФайлToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.открытьФайлToolStripMenuItem.Text = "Открыть файл";
            this.открытьФайлToolStripMenuItem.Click += new System.EventHandler(this.открытьФайлToolStripMenuItem_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(12, 27);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(505, 400);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // trackBarWidth
            // 
            this.trackBarWidth.Location = new System.Drawing.Point(3, 81);
            this.trackBarWidth.Maximum = 10000;
            this.trackBarWidth.Name = "trackBarWidth";
            this.trackBarWidth.Size = new System.Drawing.Size(349, 45);
            this.trackBarWidth.TabIndex = 2;
            this.trackBarWidth.Scroll += new System.EventHandler(this.trackBarWidth_Scroll);
            // 
            // trackBarCenter
            // 
            this.trackBarCenter.Location = new System.Drawing.Point(3, 258);
            this.trackBarCenter.Maximum = 10000;
            this.trackBarCenter.Name = "trackBarCenter";
            this.trackBarCenter.Size = new System.Drawing.Size(349, 45);
            this.trackBarCenter.TabIndex = 3;
            this.trackBarCenter.Scroll += new System.EventHandler(this.trackBarCenter_Scroll);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelCenterValue);
            this.panel1.Controls.Add(this.labelWidthValue);
            this.panel1.Controls.Add(this.labelCenterTitle);
            this.panel1.Controls.Add(this.labelWidthTitle);
            this.panel1.Controls.Add(this.trackBarWidth);
            this.panel1.Controls.Add(this.trackBarCenter);
            this.panel1.Location = new System.Drawing.Point(523, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 400);
            this.panel1.TabIndex = 4;
            // 
            // labelWidthTitle
            // 
            this.labelWidthTitle.AutoSize = true;
            this.labelWidthTitle.Location = new System.Drawing.Point(8, 65);
            this.labelWidthTitle.Name = "labelWidthTitle";
            this.labelWidthTitle.Size = new System.Drawing.Size(134, 13);
            this.labelWidthTitle.TabIndex = 4;
            this.labelWidthTitle.Text = "Параметр WindowWindth";
            // 
            // labelCenterTitle
            // 
            this.labelCenterTitle.AutoSize = true;
            this.labelCenterTitle.Location = new System.Drawing.Point(8, 242);
            this.labelCenterTitle.Name = "labelCenterTitle";
            this.labelCenterTitle.Size = new System.Drawing.Size(131, 13);
            this.labelCenterTitle.TabIndex = 5;
            this.labelCenterTitle.Text = "Параметр WindowCenter";
            // 
            // labelWidthValue
            // 
            this.labelWidthValue.AutoSize = true;
            this.labelWidthValue.Location = new System.Drawing.Point(358, 90);
            this.labelWidthValue.Name = "labelWidthValue";
            this.labelWidthValue.Size = new System.Drawing.Size(13, 13);
            this.labelWidthValue.TabIndex = 6;
            this.labelWidthValue.Text = "0";
            // 
            // labelCenterValue
            // 
            this.labelCenterValue.AutoSize = true;
            this.labelCenterValue.Location = new System.Drawing.Point(358, 267);
            this.labelCenterValue.Name = "labelCenterValue";
            this.labelCenterValue.Size = new System.Drawing.Size(13, 13);
            this.labelCenterValue.TabIndex = 7;
            this.labelCenterValue.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 435);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "DICOM-1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCenter)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьФайлToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TrackBar trackBarWidth;
        private System.Windows.Forms.TrackBar trackBarCenter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelCenterValue;
        private System.Windows.Forms.Label labelWidthValue;
        private System.Windows.Forms.Label labelCenterTitle;
        private System.Windows.Forms.Label labelWidthTitle;
        private System.Windows.Forms.Timer timer1;
    }
}

