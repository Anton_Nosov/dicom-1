﻿using EvilDICOM.Core;
using EvilDICOM.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Dicom_1
{
    static class DicomViewer
    {
        private const string MONOCHROME = "MONOCHROME";

        static public Image loadImage(string fileName, double width, double center)
        {
            var dcm = DICOMObject.Read(fileName);//читаем файл
            // так Dicom файл с теговой организацией
            // достаем информацию из необходимых нам тегов
            string photo = dcm.FindFirst(TagHelper.PhotometricInterpretation).DData.ToString();//тип изображения (монохромное или цветное и какой версии)
            ushort bitsAllocated = (ushort)dcm.FindFirst(TagHelper.BitsAllocated).DData;//сколько бит выделено
            ushort bitsStored = (ushort)dcm.FindFirst(TagHelper.BitsStored).DData;//кол-во бит в изображении
            ushort rows = (ushort)dcm.FindFirst(TagHelper.Rows).DData;//кол-во строк
            ushort colums = (ushort)dcm.FindFirst(TagHelper.Columns).DData;//кол-во столбцов
            ushort pixelRepresentation = (ushort)dcm.FindFirst(TagHelper.PixelRepresentation).DData;//пиксельное представление
            List<byte> pixelData = (List<byte>)dcm.FindFirst(TagHelper.PixelData).DData_;//список пикселей для изображения
            //работаем только с серыми изображениями
            if (!photo.Contains(MONOCHROME))
                return null;//если изображение цветное выходим из программы
            int index = 0;
            byte[] outPixelData = new byte[rows * colums * 4];//создаем массив для пикселей
            ushort mask = (ushort)(ushort.MaxValue >> (bitsAllocated - bitsStored));//маска
            double maxval = Math.Pow(2, bitsStored);//максимальное значение

            for (int i = 0; i < pixelData.Count; i += 2)
            {
                ushort gray = (ushort)(pixelData[i] + (ushort)(pixelData[i + 1] << 8));
                double valgray = gray & mask;//удаление неиспользуемых битов

                if (pixelRepresentation == 1)
                {
                    //последний бит - знак, дополняем до 2
                    if (valgray > (maxval / 2))
                        valgray -= maxval;

                }
                // Алгоритм для задания
                double half = ((width - 1) / 2.0) - 0.5;

                if (valgray <= center - half)
                    valgray = 0;
                else if (valgray >= center + half)
                    valgray = 255;
                else
                    valgray = ((valgray - (center - 0.5)) / (width - 1) + 0.5) * 255;

                outPixelData[index] = (byte)valgray;
                outPixelData[index + 1] = (byte)valgray;
                outPixelData[index + 2] = (byte)valgray;
                outPixelData[index + 3] = 255;

                index += 4;
            }


            return ImageFromRawBgraArray(outPixelData, colums, rows);
            
        }


        static public Image ImageFromRawBgraArray(
           byte[] arr, int width, int height)
        {
            var output = new Bitmap(width, height);
            var rect = new Rectangle(0, 0, width, height);
            var bmpData = output.LockBits(rect,
                ImageLockMode.ReadWrite, output.PixelFormat);
            var ptr = bmpData.Scan0;
            Marshal.Copy(arr, 0, ptr, arr.Length);
            output.UnlockBits(bmpData);
            return output;
        }
    }
}
