﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvilDICOM.Core;
using EvilDICOM.Core.Helpers;
using EvilDICOM.Network;
using EvilDICOM.RT;



namespace Dicom_1
{
    //Используем библиотеку Evil Dicom
    public partial class MainForm : Form
    {
        private double width = 0.0, center = 0.0;
        //private Image image;
        private string fileName = "";

        public MainForm()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void trackBarWidth_Scroll(object sender, EventArgs e)
        {
            // переводим значение, установившееся в элементе trackBar в необходимый нам формат 
            width = (double)trackBarWidth.Value;
            // подписываем это значение в label элементе под данным ползунком 
            labelWidthValue.Text = width.ToString();
        }

        private void trackBarCenter_Scroll(object sender, EventArgs e)
        {
            // переводим значение, установившееся в элементе trackBar в необходимый нам формат 
            center = (double)trackBarCenter.Value;
            // подписываем это значение в label элементе под данным ползунком 
            labelCenterValue.Text = center.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!fileName.Equals(""))
                draw();
        }

        private void draw()
        {
            pictureBox.Image = DicomViewer.loadImage(fileName, width, center);
        }

        private void открытьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog.FileName;              
            }
        }

       
    }
}
